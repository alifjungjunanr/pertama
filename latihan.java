import java.util.Scanner;

class Latihan1 {
	public static void main(String args[]) {
		Scanner sc= new Scanner(System.in);
		String nama;
		int umur;

		System.out.print("Masukkan Nama : "); 
		nama=sc.next();
		System.out.print("Masukkan Umur : "); 
		umur=sc.nextInt();
		System.out.println("Anda bernama "+nama+" berumur "+umur+" tahun");
	}
}
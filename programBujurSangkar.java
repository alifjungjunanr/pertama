import java.util.Scanner;

public class BujurSangkarCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukkan panjang sisi bujur sangkar: ");
        double sisi = scanner.nextDouble();

        double luas = hitungLuasBujurSangkar(sisi);
        double keliling = hitungKelilingBujurSangkar(sisi);

        System.out.println("Luas bujur sangkar: " + luas);
        System.out.println("Keliling bujur sangkar: " + keliling);
    }

    public static double hitungLuasBujurSangkar(double sisi) {
        return sisi * sisi;
    }

    public static double hitungKelilingBujurSangkar(double sisi) {
        return 4 * sisi;
    }
}
